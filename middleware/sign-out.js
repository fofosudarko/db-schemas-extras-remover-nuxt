import routes from '~/config/routes';

export default async function({ store, redirect }) {
  if (process.client) {
    window.localStorage.removeItem('loggedUser');
    window.localStorage.removeItem('getTable');
    window.localStorage.removeItem('searchTable');

    await store
      .dispatch('deleteAccessToken', {})
      .catch(error => console.error(error));

    store.commit('RESET');

    return redirect(routes.SIGN_IN);
  }
}
