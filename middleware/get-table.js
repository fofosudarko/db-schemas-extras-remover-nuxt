// get-table.js

import routes from '~/config/routes';

export default function({ store, redirect }) {
  let loggedUser = null;

  if (process.client) {
    loggedUser = store.getters.loggedUser;

    if (!loggedUser) {
      loggedUser = window.localStorage.getItem('loggedUser');

      if (!(loggedUser && loggedUser.length)) {
        return redirect(routes.SIGN_IN);
      }

      loggedUser = JSON.parse(loggedUser);
      store.commit('SET_LOGGEDUSER', loggedUser);
      store.commit('SET_USERLOGGEDIN', true);
    } else return;
  }
}
