// search-table.js

import routes from '~/config/routes';

export default function({ redirect, store }) {
  let loggedUser = null;

  if (process.client) {
    loggedUser = window.localStorage.getItem('loggedUser');

    if (!(loggedUser && loggedUser.length)) {
      return redirect(routes.SIGN_IN);
    } else {
      store.commit('SET_LOGGEDUSER', JSON.parse(loggedUser));
      store.commit('SET_USERLOGGEDIN', true);
    }

    let getTable = store.getters.getTable;

    if (!getTable) {
      getTable = window.localStorage.getItem('getTable');

      if (getTable) {
        store.commit('SET_GETTABLE', JSON.parse(getTable));
      } else return redirect(routes.GET_TABLE);
    }

    return;
  }
}
