// store.js

import services from '~/services/services';
import utils from '~/utils/utils';
import httpStatus from 'http-status';
import { APP_BASEURI, SMARTSAPP_BASEURI } from '~/config/config';

const state = () => ({
  loggedUser: null,
  userLoggedIn: null,
  getTables: null,
  getTable: null,
  searchTable: null,
  deleteRows: null,
  queryTableAttributes: null,
  filteredTableAttributes: null,
  accessToken: null,
  deleteAccessToken: null
});

const mutations = {
  SET_LOGGEDUSER(state, loggedUser) {
    state.loggedUser = loggedUser;
  },
  SET_USERLOGGEDIN(state, userLoggedIn) {
    state.userLoggedIn = userLoggedIn;
  },
  SET_GETTABLE(state, getTable) {
    state.getTable = getTable;
  },
  SET_GETTABLES(state, getTables) {
    state.getTables = getTables;
  },
  SET_SEARCHTABLE(state, searchTable) {
    state.searchTable = searchTable;
  },
  SET_DELETEROWS(state, deleteRows) {
    state.deleteRows = deleteRows;
  },
  SET_QUERYTABLEATTRIBUTES(state, queryTableAttributes) {
    state.queryTableAttributes = queryTableAttributes;
  },
  SET_FILTEREDTABLEATTRIBUTES(state, filteredTableAttributes) {
    state.filteredTableAttributes = filteredTableAttributes;
  },
  SET_ACCESSTOKEN(state, accessToken) {
    state.accessToken = accessToken;
  },
  SET_DELETEACCESSTOKEN(state, deleteAccessToken) {
    state.deleteAccessToken = deleteAccessToken;
  },
  RESET(state) {
    let properties = Object.keys(state);

    if (properties && properties.length) {
      properties.forEach(property => {
        state[property] = null;
      });
    }
  }
};

const actions = {
  async login(store, { data }) {
    try {
      let { VERB, URI } = services.LOGIN;
      let serviceUriParams = {
        data: data,
        headers: { 'X-Request-Platform': 'WEB' }
      };
      let config = utils.getServiceUriConfig(
        SMARTSAPP_BASEURI,
        VERB,
        URI,
        serviceUriParams
      );
      let response = await this.$axios.request(config);

      if (response) {
        switch (response.status) {
          case httpStatus.OK:
            store.commit('SET_LOGGEDUSER', response.data);
            break;
          case httpStatus.NOT_FOUND:
          case httpStatus.METHOD_NOT_ALLOWED:
          case httpStatus.INTERNAL_SERVER_ERROR:
          case httpStatus.UNAUTHORIZED:
            store.commit('SET_LOGGEDUSER', null);
            store.commit('SET_USERLOGGEDIN', false);
            break;
        }
      }
    } catch (error) {
      console.error(error);
      throw error;
    }
  },
  async getTables(store, {}) {
    try {
      let { VERB, URI } = services.GET_TABLES;
      let accessToken = store.getters.accessToken;
      let serviceUriParams = {
        headers: Object.assign(
          { 'X-Request-Platform': 'WEB' },
          Object.assign(
            utils.getAuthorizationHeader(accessToken),
            utils.getAuthorizationKey(accessToken)
          )
        )
      };
      let config = utils.getServiceUriConfig(
        APP_BASEURI,
        VERB,
        URI,
        serviceUriParams
      );
      let response = await this.$axios.request(config);

      if (response) {
        switch (response.status) {
          case httpStatus.OK:
            store.commit('SET_GETTABLES', response.data);
            break;
          case httpStatus.NOT_FOUND:
          case httpStatus.METHOD_NOT_ALLOWED:
          case httpStatus.INTERNAL_SERVER_ERROR:
            store.commit('SET_GETTABLES', null);
            break;
        }
      }
    } catch (error) {
      console.error(error);
      throw error;
    }
  },
  async getTable(store, { data }) {
    try {
      let { VERB, URI } = services.GET_TABLE;
      let accessToken = store.getters.accessToken;
      let serviceUriParams = {
        data: data,
        headers: Object.assign(
          { 'X-Request-Platform': 'WEB' },
          Object.assign(
            utils.getAuthorizationHeader(accessToken),
            utils.getAuthorizationKey(accessToken)
          )
        )
      };
      let config = utils.getServiceUriConfig(
        APP_BASEURI,
        VERB,
        URI,
        serviceUriParams
      );
      let response = await this.$axios.request(config);

      if (response) {
        switch (response.status) {
          case httpStatus.OK:
            store.commit('SET_GETTABLE', response.data);
            break;
          case httpStatus.NOT_FOUND:
          case httpStatus.METHOD_NOT_ALLOWED:
          case httpStatus.INTERNAL_SERVER_ERROR:
            store.commit('SET_GETTABLE', null);
            break;
        }
      }
    } catch (error) {
      console.error(error);
      throw error;
    }
  },
  async searchTable(store, { data }) {
    try {
      let { VERB, URI } = services.SEARCH_TABLE;
      let accessToken = store.getters.accessToken;
      let serviceUriParams = {
        data: data,
        headers: Object.assign(
          { 'X-Request-Platform': 'WEB' },
          Object.assign(
            utils.getAuthorizationHeader(accessToken),
            utils.getAuthorizationKey(accessToken)
          )
        )
      };
      let config = utils.getServiceUriConfig(
        APP_BASEURI,
        VERB,
        URI,
        serviceUriParams
      );
      let response = await this.$axios.request(config);

      if (response) {
        switch (response.status) {
          case httpStatus.OK:
            store.commit('SET_SEARCHTABLE', response.data);
            break;
          case httpStatus.BAD_REQUEST:
          case httpStatus.NOT_FOUND:
          case httpStatus.METHOD_NOT_ALLOWED:
          case httpStatus.INTERNAL_SERVER_ERROR:
            store.commit('SET_SEARCHTABLE', null);
            break;
        }
      }
    } catch (error) {
      console.error(error);
      throw error;
    }
  },
  async deleteRows(store, { data }) {
    try {
      let { VERB, URI } = services.DELETE_ROWS;
      let accessToken = store.getters.accessToken;
      let serviceUriParams = {
        data: data,
        headers: Object.assign(
          { 'X-Request-Platform': 'WEB' },
          Object.assign(
            utils.getAuthorizationHeader(accessToken),
            utils.getAuthorizationKey(accessToken)
          )
        )
      };
      let config = utils.getServiceUriConfig(
        APP_BASEURI,
        VERB,
        URI,
        serviceUriParams
      );
      let response = await this.$axios.request(config);

      if (response) {
        switch (response.status) {
          case httpStatus.NO_CONTENT:
            store.commit('SET_DELETEROWS', true);
            break;
          case httpStatus.NOT_FOUND:
          case httpStatus.METHOD_NOT_ALLOWED:
          case httpStatus.INTERNAL_SERVER_ERROR:
            store.commit('SET_DELETEROWS', false);
            break;
        }
      }
    } catch (error) {
      console.error(error);
      throw error;
    }
  },
  async deleteAccessToken(store, {}) {
    try {
      let { VERB, URI } = services.DELETE_TOKEN;
      let accessToken = store.getters.accessToken;
      let serviceUriParams = {
        headers: Object.assign(
          { 'X-Request-Platform': 'WEB' },
          Object.assign(
            utils.getAuthorizationHeader(accessToken),
            utils.getAuthorizationKey(accessToken)
          )
        )
      };
      let config = utils.getServiceUriConfig(
        APP_BASEURI,
        VERB,
        URI,
        serviceUriParams
      );
      let response = await this.$axios.request(config);

      if (response) {
        switch (response.status) {
          case httpStatus.NO_CONTENT:
            store.commit('SET_DELETEACCESSTOKEN', true);
            break;
          case httpStatus.NOT_FOUND:
          case httpStatus.METHOD_NOT_ALLOWED:
          case httpStatus.INTERNAL_SERVER_ERROR:
            store.commit('SET_DELETEACCESSTOKEN', false);
            break;
        }
      }
    } catch (error) {
      console.error(error);
      throw error;
    }
  }
};

const getters = {
  loggedUser(state) {
    return state.loggedUser;
  },
  userLoggedIn(state) {
    return state.userLoggedIn;
  },
  getTable(state) {
    return state.getTable;
  },
  getTables(state) {
    return state.getTables;
  },
  searchTable(state) {
    return state.searchTable;
  },
  deleteRows(state) {
    return state.deleteRows;
  },
  queryTableAttributes(state) {
    return state.queryTableAttributes;
  },
  filteredTableAttributes(state) {
    return state.filteredTableAttributes;
  },
  accessToken(state) {
    return state.accessToken;
  },
  deleteAccessToken(state) {
    return state.deleteAccessToken;
  }
};

export default {
  state,
  actions,
  mutations,
  getters
};
