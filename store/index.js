// index.js

import Vuex from 'vuex';
import AppModule from '~/store/module';

const createStore = () => {
  return new Vuex.Store({
    modules: {
      AppModule
    }
  });
};

export default createStore;
