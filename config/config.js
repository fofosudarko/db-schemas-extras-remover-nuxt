// config.js

export const SCHEMES = {
  APP: 'https',
  SMARTSAPP: 'https'
};

export const HOSTS = {
  APP: 'apps.minex360.com',
  SMARTSAPP: 'smartadmin.minex360.com'
};

export const PORTS = {
  APP: '',
  SMARTSAPP: ''
};

export const APP_BASEURI = `${SCHEMES.APP}://${HOSTS.APP}:${PORTS.APP}`;

export const SMARTSAPP_BASEURI = `${SCHEMES.SMARTSAPP}://${HOSTS.SMARTSAPP}:${
  PORTS.SMARTSAPP
}`;

export const TABLES = {
  STUDENTS: 'public.students',
  PARENTS: 'public.parents'
};

export const TABLE_FORMS = [...Object.values(TABLES)];

export const TABLE_HEADERS = {
  [TABLES.PARENTS]: {
    avatar: 'Avatar',
    created_date: 'Created Date',
    delegate_type: 'Delegate Type',
    email: 'Email',
    first_name: 'First Name',
    id: 'ID',
    last_name: 'Last Name',
    other_names: 'Other Names',
    parent_type: 'Parent Type',
    primary_contact: 'Primary Contact',
    school_id: 'School ID',
    secondary_contact: 'Secondary Contact',
    status: 'Status',
    updated_date: 'Updated Date'
  },
  [TABLES.STUDENTS]: {
    id: 'ID',
    grade_id: 'Grade ID',
    school_id: 'School ID',
    first_name: 'First Name',
    last_name: 'Last Name',
    other_names: 'Other Names',
    last_pickup_datetime: 'Last Pickup Datetime',
    last_pickup_delegate: 'Last Pickup Delegate',
    last_drop_off_datetime: 'Last Drop Off Datetime',
    last_drop_off_delegate: 'Last Drop Off Delegate',
    dob: 'Date Of Birth',
    gender: 'Gender',
    bloodgroup: 'Blood Group',
    nationality: 'Nationality',
    avatar: 'Avatar',
    created_date: 'Created Date',
    updated_date: 'Updated Date',
    status: 'Status',
    configuration_id: 'Configuration ID',
    campus_id: 'Campus ID',
    delegate_id: 'Delegate ID',
    student_number: 'Student Number',
    delegate_ref_key: 'Delegate Reference Key'
  },
  [TABLES.APP_USERS]: {
    id: 'ID',
    delegate_id: 'Delegate ID',
    environment_id: 'Environment ID',
    role_id: 'Role ID',
    mobile_number: 'Mobile Number',
    pin: 'PIN',
    verified: 'Verified',
    status: 'Status',
    last_seen: 'Last Seen',
    created_date: 'Created Date',
    updated_date: 'Updated Date',
    school_id: 'School ID'
  },
  [TABLES.CAMPUSES]: {
    id: 'ID',
    environment_id: 'Environment ID',
    school_id: 'School ID',
    name: 'Name',
    alias: 'Alias',
    location: 'Location',
    contact: 'Contact',
    address: 'Address',
    created_date: 'Created Date',
    updated_date: 'Updated Date',
    configuration_id: 'Configuration ID'
  },
  [TABLES.DELEGATES]: {
    id: 'ID',
    delegate_type: 'Delegate Type',
    primary_contact: 'Primary Contact',
    is_primary: 'Is Primary',
    full_name: 'Full Name',
    relation: 'Relation',
    gender: 'Gender',
    last_geo_fence_entry_datetime: 'Last Geo Fence Entry Datetime',
    last_geo_fence_exit_datetime: 'Last Geo Fence Exit Datetime',
    created_date: 'Created Date',
    updated_date: 'Updated Date',
    last_geo_fence_entry_latitude: 'Last Geo Fence Entry Latitude',
    last_geo_fence_entry_longitude: 'Last Geo Fence Entry Longitude',
    last_geo_fence_exit_latitude: 'Last Geo Fence Exit Latitude',
    last_geo_fence_exit_longitude: 'Last Geo Fence Exit Longitude',
    avatar: 'Avatar',
    created_by: 'Created By',
    school_id: 'School ID'
  },
  [TABLES.EMPLOYEES]: {
    id: 'ID',
    school_id: 'School ID',
    first_name: 'First Name',
    last_name: 'Last Name',
    primary_contact: 'Primary Contact',
    other_names: 'Other Names',
    created_date: 'Created Date',
    updated_date: 'Updated Date',
    email: 'Email',
    role_id: 'Role ID',
    avatar: 'Avatar',
    status: 'Status',
    campus_id: 'Campus ID'
  },
  [TABLES.PORTAL_USERS]: {
    id: 'ID',
    school_id: 'School ID',
    employee_id: 'Employee ID',
    role_id: 'Role ID',
    environment_id: 'Environment ID',
    email: 'Email',
    password: 'Password',
    verified: 'Verified',
    status: 'Status',
    last_seen: 'Last Seen',
    created_date: 'Created Date',
    updated_date: 'Updated Date'
  },
  [TABLES.GRADES]: {
    id: 'ID',
    school_id: 'School ID',
    code: 'Code',
    name: 'Name',
    description: 'Description',
    created_date: 'Created Date',
    updated_date: 'Updated Date',
    student_count: 'Student Count',
    campus_id: 'Campus ID',
    configuration_id: 'Configuration ID',
    level: 'Level'
  }
};

export const TABLE_ATTRIBUTES_FILTER = {
  [TABLES.PARENTS]: [
    'created_date',
    'delegate_type',
    'id',
    'school_id',
    'updated_date'
  ],
  [TABLES.STUDENTS]: [
    'id',
    'grade_id',
    'school_id',
    'last_pickup_datetime',
    'last_pickup_delegate',
    'last_drop_off_datetime',
    'last_drop_off_delegate',
    'created_date',
    'updated_date',
    'configuration_id',
    'campus_id',
    'delegate_id',
    'delegate_ref_key'
  ],
  [TABLES.APP_USERS]: [
    'id',
    'delegate_id',
    'environment_id',
    'role_id',
    'created_date',
    'updated_date',
    'school_id'
  ],
  [TABLES.PORTAL_USERS]: [
    'id',
    'school_id',
    'employee_id',
    'role_id',
    'environment_id',
    'created_date',
    'updated_date'
  ],
  [TABLES.DELEGATES]: [
    'id',
    'last_geo_fence_entry_datetime',
    'last_geo_fence_exit_datetime',
    'created_date',
    'updated_date',
    'last_geo_fence_entry_latitude',
    'last_geo_fence_entry_longitude',
    'last_geo_fence_exit_latitude',
    'last_geo_fence_exit_longitude',
    'school_id'
  ],
  [TABLES.EMPLOYEES]: [
    'id',
    'school_id',
    'created_date',
    'updated_date',
    'campus_id'
  ],
  [TABLES.GRADES]: [
    'id',
    'school_id',
    'created_date',
    'updated_date',
    'campus_id',
    'configuration_id'
  ],
  [TABLES.CAMPUSES]: [
    'id',
    'environment_id',
    'school_id',
    'created_date',
    'updated_date',
    'configuration_id'
  ]
};

export const APPLICATION_NAME = 'SmartsApp Filter Services';

export const GET_TABLES_FILTER = [
  'public.parents',
  'public.students',
  'public.app_users',
  'public.portal_users',
  'public.delegates',
  'public.employees',
  'public.grades',
  'public.campuses'
];
