// apis.js

const APP_BACKEND_API = '/db-schemas-extras-remover/postgresql';
const SMARTSAPP_BACKEND_API = '';

export default {
  APP_BACKEND_API,
  SMARTSAPP_BACKEND_API
};
