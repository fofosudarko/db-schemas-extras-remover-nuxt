// routes.js

const HOME = '/home';
const GET_TABLE = '/get-table';
const SEARCH_TABLE = '/search-table';
const SIGN_OUT = '/sign-out';
const SIGN_IN = '/sign-in';

export default {
  HOME,
  GET_TABLE,
  SEARCH_TABLE,
  SIGN_OUT,
  SIGN_IN
};
