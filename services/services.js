// services.js

import apis from '~/config/apis';

const APP_BASE = apis.APP_BACKEND_API;
const SMARTSAPP_BASE = apis.SMARTSAPP_BACKEND_API;

const HTTP_VERBS = {
  GET: 'GET',
  POST: 'POST',
  DELETE: 'DELETE'
};

const LOGIN = {
  VERB: HTTP_VERBS.POST,
  URI: `${SMARTSAPP_BASE}/login`
};

const GET_TABLES = {
  VERB: HTTP_VERBS.GET,
  URI: `${APP_BASE}/get-tables`
};

const GET_TABLE = {
  VERB: HTTP_VERBS.POST,
  URI: `${APP_BASE}/get-table`
};

const SEARCH_TABLE = {
  VERB: HTTP_VERBS.POST,
  URI: `${APP_BASE}/search-table`
};

const DELETE_ROWS = {
  VERB: HTTP_VERBS.DELETE,
  URI: `${APP_BASE}/delete-rows`
};

const DELETE_TOKEN = {
  VERB: HTTP_VERBS.DELETE,
  URI: `${APP_BASE}/delete-token`
};

export default {
  LOGIN,
  GET_TABLES,
  GET_TABLE,
  SEARCH_TABLE,
  DELETE_ROWS,
  DELETE_TOKEN
};
