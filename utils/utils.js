// utils.js

export const getBaseUri = () => {
  return BASEURI;
};

export const getServiceUriConfig = (
  baseUri,
  verb,
  serviceUri,
  { params, data, headers, config }
) => {
  let serviceBaseUri = baseUri;
  let uriConfig = {
    url: serviceUri,
    method: verb,
    baseURL: serviceBaseUri
  };

  if (config) {
    uriConfig = Object.assign(uriConfig, config);
  }

  if (params) {
    uriConfig.params = params;
  }

  if (data) {
    uriConfig.data = data;
  }

  if (headers) {
    uriConfig.headers = headers;
  }

  return uriConfig;
};

export const batchCollection = (collection = [], rows, cols) => {
  let batchedCollection = [];
  let start = 0,
    end = cols;

  for (let row = 1; row <= rows; row++) {
    batchedCollection.push(collection.slice(start, end));
    start = end;
    end += cols;
  }

  return batchedCollection;
};

export const getAuthorizationHeader = (accessToken = '') => ({
  Authorization: `Bearer ${accessToken}`
});

export const getAuthorizationKey = (accessToken = '') => ({
  'X-App-Access-Token': accessToken ? accessToken.substring(0, 20) : ''
});

export const removeDashes = (text = '') => {
  return text.replace(/_/g, ' ');
};

export const removeSchema = (table = '') => {
  return table.replace(/^public\.?/, '');
};

export const capitalize = (text = '') =>
  text.charAt(0).toLocaleUpperCase() + text.substring(1);

export default {
  getBaseUri,
  getServiceUriConfig,
  batchCollection,
  getAuthorizationHeader,
  getAuthorizationKey,
  removeDashes,
  removeSchema,
  capitalize
};
